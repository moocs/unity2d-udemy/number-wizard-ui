﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class NumberWizard : MonoBehaviour
{
    [SerializeField] int minimum = 1;
    [SerializeField] int maximum = 100;
    [SerializeField] private TextMeshProUGUI guessText;

    private int guess;

    // Start is called before the first frame update
    void Start()
    {
        StartGame();
    }

    void StartGame()
    {
        // Expanding max and min so the max and min values could be obtained (otherwise, blocking at min +1)
        NextGuess();
        maximum++;
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void OnPressHigher()
    {
        minimum = guess + 1;
        NextGuess();
    }

    public void OnPressLower()
    {
        maximum = guess - 1;
        NextGuess();
    }

    private void NextGuess()
    {
        guess = Random.Range(minimum, maximum + 1);
        guessText.text = guess.ToString();
    }
}